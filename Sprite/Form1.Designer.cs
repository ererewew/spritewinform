﻿namespace Sprite
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decreseSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.increaseSpeedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decreaseSpeedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopMovingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.continueMovingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeCharacterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aToolStripMenuItem,
            this.decreseSizeToolStripMenuItem,
            this.increaseSpeedToolStripMenuItem,
            this.decreaseSpeedToolStripMenuItem,
            this.stopMovingToolStripMenuItem,
            this.continueMovingToolStripMenuItem,
            this.changeCharacterToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(168, 180);
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aToolStripMenuItem.Text = "Increase size";
            this.aToolStripMenuItem.Click += new System.EventHandler(this.aToolStripMenuItem_Click);
            // 
            // decreseSizeToolStripMenuItem
            // 
            this.decreseSizeToolStripMenuItem.Name = "decreseSizeToolStripMenuItem";
            this.decreseSizeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.decreseSizeToolStripMenuItem.Text = "Decrese size";
            this.decreseSizeToolStripMenuItem.Click += new System.EventHandler(this.decreseSizeToolStripMenuItem_Click);
            // 
            // increaseSpeedToolStripMenuItem
            // 
            this.increaseSpeedToolStripMenuItem.Name = "increaseSpeedToolStripMenuItem";
            this.increaseSpeedToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.increaseSpeedToolStripMenuItem.Text = "Increase speed";
            this.increaseSpeedToolStripMenuItem.Click += new System.EventHandler(this.increaseSpeedToolStripMenuItem_Click);
            // 
            // decreaseSpeedToolStripMenuItem
            // 
            this.decreaseSpeedToolStripMenuItem.Name = "decreaseSpeedToolStripMenuItem";
            this.decreaseSpeedToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.decreaseSpeedToolStripMenuItem.Text = "Decrease speed";
            this.decreaseSpeedToolStripMenuItem.Click += new System.EventHandler(this.decreaseSpeedToolStripMenuItem_Click);
            // 
            // stopMovingToolStripMenuItem
            // 
            this.stopMovingToolStripMenuItem.Name = "stopMovingToolStripMenuItem";
            this.stopMovingToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.stopMovingToolStripMenuItem.Text = "Stop moving";
            this.stopMovingToolStripMenuItem.Click += new System.EventHandler(this.stopMovingToolStripMenuItem_Click);
            // 
            // continueMovingToolStripMenuItem
            // 
            this.continueMovingToolStripMenuItem.Name = "continueMovingToolStripMenuItem";
            this.continueMovingToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.continueMovingToolStripMenuItem.Text = "Continue moving";
            this.continueMovingToolStripMenuItem.Click += new System.EventHandler(this.continueMovingToolStripMenuItem_Click);
            // 
            // changeCharacterToolStripMenuItem
            // 
            this.changeCharacterToolStripMenuItem.Name = "changeCharacterToolStripMenuItem";
            this.changeCharacterToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.changeCharacterToolStripMenuItem.Text = "Change character";
            this.changeCharacterToolStripMenuItem.Click += new System.EventHandler(this.changeCharacterToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.Form1_PreviewKeyDown);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decreseSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem increaseSpeedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decreaseSpeedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopMovingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem continueMovingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeCharacterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}

