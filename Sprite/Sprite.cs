﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Sprite
{
    enum Direction {TOP = 0, DOWN, LEFT, RIGHT, NOT_SPECIFY};
    class Sprite
    {
        private Bitmap[][] bmps; // top down left right
        private int iFrame = 0;
        private int[] nFrame = new int[4];  // top down left right - max frame
        private Direction currentDirection = Direction.DOWN;

        // coordinate to draw
        public int top { get; set;}
        public int left { get; set;}
        public int movingSpeed { get; set; } = 1;
        public bool moveWhileStanding { get; set; } = true;

        private int height;
        private int width;
        
        private float scale;
        public float Scale
        {
            set
            {
                scale = value;
                height = (int)(scale * bmps[0][0].Height);
                width = (int)(scale * bmps[0][0].Width);
            }
            get
            {
                return scale;
            }
        }

        public Sprite(Bitmap[][] bmps, int top, int left, int[] nFrame)
        {
            this.bmps = bmps;
            this.top = top;
            this.left = left;
            this.nFrame = nFrame;
            this.height = bmps[0][0].Height;
            this.width = bmps[0][0].Width;
            this.Scale = 1;
        }

        public void Move(Direction direction, bool changeCoordinate)
        {
            if (direction == Direction.NOT_SPECIFY)
                direction = currentDirection;
            else
                currentDirection = direction;
            if (changeCoordinate)
            {
                // change coordinate
                switch (direction)
                {
                    case Direction.TOP:
                        top -= movingSpeed;
                        break;
                    case Direction.DOWN:
                        top += movingSpeed;
                        break;
                    case Direction.LEFT:
                        left -= movingSpeed;
                        break;
                    default: // right
                        left += movingSpeed;
                        break;
                }
            }

            // increase frame index
            if (changeCoordinate || moveWhileStanding)
                iFrame = (iFrame + 1) % nFrame[(int)direction];
        }
        public void Draw(Graphics g)
        {
            // draw
            //g.DrawImage(bmps[(int)currentDirection][iFrame[(int)currentDirection]], left, top);
            g.DrawImage(bmps[(int)currentDirection][iFrame], new Rectangle(left,top, height, width));
        }
    }
}
