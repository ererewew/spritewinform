﻿namespace Sprite
{
    partial class CharacterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioProfessor = new System.Windows.Forms.RadioButton();
            this.radioBlone = new System.Windows.Forms.RadioButton();
            this.radioKid = new System.Windows.Forms.RadioButton();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // radioProfessor
            // 
            this.radioProfessor.AutoSize = true;
            this.radioProfessor.Checked = true;
            this.radioProfessor.Location = new System.Drawing.Point(38, 38);
            this.radioProfessor.Name = "radioProfessor";
            this.radioProfessor.Size = new System.Drawing.Size(69, 17);
            this.radioProfessor.TabIndex = 0;
            this.radioProfessor.TabStop = true;
            this.radioProfessor.Text = "Professor";
            this.radioProfessor.UseVisualStyleBackColor = true;
            // 
            // radioBlone
            // 
            this.radioBlone.AutoSize = true;
            this.radioBlone.Location = new System.Drawing.Point(281, 38);
            this.radioBlone.Name = "radioBlone";
            this.radioBlone.Size = new System.Drawing.Size(52, 17);
            this.radioBlone.TabIndex = 1;
            this.radioBlone.Text = "Blone";
            this.radioBlone.UseVisualStyleBackColor = true;
            // 
            // radioKid
            // 
            this.radioKid.AutoSize = true;
            this.radioKid.Location = new System.Drawing.Point(168, 38);
            this.radioKid.Name = "radioKid";
            this.radioKid.Size = new System.Drawing.Size(40, 17);
            this.radioKid.TabIndex = 2;
            this.radioKid.Text = "Kid";
            this.radioKid.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(150, 85);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // CharacterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 120);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.radioKid);
            this.Controls.Add(this.radioBlone);
            this.Controls.Add(this.radioProfessor);
            this.Name = "CharacterForm";
            this.Text = "CharacterForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CharacterForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioProfessor;
        private System.Windows.Forms.RadioButton radioBlone;
        private System.Windows.Forms.RadioButton radioKid;
        private System.Windows.Forms.Button btnOK;
    }
}