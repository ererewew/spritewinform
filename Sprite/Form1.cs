﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sprite
{
    public partial class Form1 : Form
    {
        private Bitmap[][] bmps = new Bitmap[4][]; // top down left right - Bitmaps after decomposing from sprite
        private Bitmap sprite; // sprite sheet
        Sprite character;
        public Form1()
        {
            InitializeComponent();
            
            // load sprite sheet 
            sprite = Properties.Resources.professor;

            // decompose to separate bitmaps
            DecomposeSprite(sprite, ref bmps[0], 9, 64, 64, 0, 0);
            DecomposeSprite(sprite, ref bmps[2], 9, 64, 64, 0, 64);
            DecomposeSprite(sprite, ref bmps[1], 9, 64, 64, 0, 64 + 64);
            DecomposeSprite(sprite, ref bmps[3], 9, 64, 64, 0, 64 + 64 + 64);

            
            character = new Sprite(bmps, 0, 0, new int[] {9,9,9,9});

            sprite.Dispose();
        }


        private void DecomposeSprite(Bitmap sprite, ref Bitmap[] bmp, int numFrames, int frameWidth, int frameHeight, int left, int top)
        {
            bmp = new Bitmap[numFrames];
            for (int i =0; i < numFrames; ++i)
                bmp[i] = sprite.Clone(new Rectangle(left + i * frameWidth, top, frameWidth, frameHeight), System.Drawing.Imaging.PixelFormat.DontCare);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            character.Draw(e.Graphics);
        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    timer1.Stop();
                    character.Move(Direction.TOP, true);
                    break;
                case Keys.Down:
                    timer1.Stop();
                    character.Move(Direction.DOWN, true);
                    break;
                case Keys.Left:
                    timer1.Stop();
                    character.Move(Direction.LEFT, true);
                    break;
                case Keys.Right:
                    timer1.Stop();
                    character.Move(Direction.RIGHT, true);
                    break;
                case Keys.Oemplus:
                    character.movingSpeed = character.movingSpeed + 2;
                    break;
                case Keys.OemMinus:
                    if (character.movingSpeed - 2 > 0)
                        character.movingSpeed = character.movingSpeed - 2;
                    break;
                case Keys.OemOpenBrackets:
                    character.Scale = Math.Max(character.Scale - 0.1f, 0);
                    break;
                case Keys.OemCloseBrackets:
                    character.Scale = character.Scale + 0.1f;
                    break;
                case Keys.S:
                    character.moveWhileStanding = false;
                    break;
                case Keys.P:
                    character.moveWhileStanding = true;
                    break;
                case Keys.Escape:
                    Close();
                    break;
            }
            this.Refresh();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            character.Move(Direction.NOT_SPECIFY, false);
            Invalidate();
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
                timer1.Start();
        }

        private void Form1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            e.IsInputKey = true;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                character.left = e.X;
                character.top = e.Y;
            }

        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                contextMenuStrip1.Show(this, e.X, e.Y);
        }

        private void aToolStripMenuItem_Click(object sender, EventArgs e)
        {
            character.Scale = character.Scale + 0.1f;
        }

        private void decreseSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            character.Scale = Math.Max(character.Scale - 0.1f, 0);
        }

        private void increaseSpeedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            character.movingSpeed = character.movingSpeed + 2;
        }

        private void decreaseSpeedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (character.movingSpeed - 2 > 0)
                character.movingSpeed = character.movingSpeed - 2;
        }

        private void stopMovingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            character.moveWhileStanding = false;
        }

        private void continueMovingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            character.moveWhileStanding = true;
        }

        private void changeCharacterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CharacterForm characterForm = new CharacterForm();
            characterForm.ShowDialog(this);
            
            switch (characterForm.chosenCharacter)
            {
                case Character.PROFESSOR:
                    // load sprite sheet 
                    sprite = Properties.Resources.professor;

                    // decompose to separate bitmaps
                    DecomposeSprite(sprite, ref bmps[0], 9, 64, 64, 0, 0);
                    DecomposeSprite(sprite, ref bmps[2], 9, 64, 64, 0, 64);
                    DecomposeSprite(sprite, ref bmps[1], 9, 64, 64, 0, 64 + 64);
                    DecomposeSprite(sprite, ref bmps[3], 9, 64, 64, 0, 64 + 64 + 64);

                    sprite.Dispose();
                    
                    character = new Sprite(bmps, 0, 0, new int[] {9,9,9,9});
                    break;
                case Character.BLONE:
                    // load sprite sheet 
                    sprite = Properties.Resources.blone;

                    // decompose to separate bitmaps
                    DecomposeSprite(sprite, ref bmps[1], 8, 24, 32, 0, 0);
                    DecomposeSprite(sprite, ref bmps[0], 8, 24, 32, 0, 32);
                    DecomposeSprite(sprite, ref bmps[2], 8, 24, 32, 0, 32 + 32);
                    DecomposeSprite(sprite, ref bmps[3], 8, 24, 32, 0, 32 + 32 + 32);

                    sprite.Dispose();

                    character = new Sprite(bmps, 0, 0, new int[] {8,8,8,8});

                    break;
                case Character.KID:
                    // load sprite sheet 
                    sprite = Properties.Resources.kid;

                    // decompose to separate bitmaps
                    DecomposeSprite(sprite, ref bmps[1], 4, 64, 64, 0, 0);
                    DecomposeSprite(sprite, ref bmps[2], 4, 64, 64, 0, 64);
                    DecomposeSprite(sprite, ref bmps[3], 4, 64, 64, 0, 64 + 64);
                    DecomposeSprite(sprite, ref bmps[0], 4, 64, 64, 0, 64 + 64 + 64);

                    sprite.Dispose();

                    character = new Sprite(bmps, 0, 0, new int[] {4,4,4,4});

                    break;


            }
            characterForm.Dispose();
            ActiveControl = null;

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
