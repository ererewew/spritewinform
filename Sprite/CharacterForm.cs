﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sprite
{
    public enum Character { PROFESSOR, BLONE, KID };
    public partial class CharacterForm : Form
    {
        public Character chosenCharacter { get; set; }
        public CharacterForm()
        {
            InitializeComponent();
        }

        private void CharacterForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (radioProfessor.Checked)
                chosenCharacter = Character.PROFESSOR;
            else
                if (radioBlone.Checked)
                chosenCharacter = Character.BLONE;
            else
                if (radioKid.Checked)
                chosenCharacter = Character.KID;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
